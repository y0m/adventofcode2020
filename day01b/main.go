package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"strconv"
)

func main() {
	b, err := ioutil.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}
	buffer := bytes.NewBuffer(b)
	sc := bufio.NewScanner(buffer)
	expenses := make([]int, 0)
	for sc.Scan() {
		i, err := strconv.Atoi(sc.Text())
		if err != nil {
			panic(err)
		}
		expenses = append(expenses, i)
	}

	for i, a := range expenses {
		for j, b := range expenses {
			for k, c := range expenses {
				if i != j && j != k && (a+b+c) == 2020 {
					fmt.Printf("%d + %d + %d = 2020\n%d * %d * %d = %d\n",
						a, b, c,
						a, b, c, a*b*c)
					goto found
				}
			}

		}
	}
found:
}
