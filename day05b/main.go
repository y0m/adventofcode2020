package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	var seats [128][8]int
	sc := bufio.NewScanner(f)
	var highest int
	for sc.Scan() {
		row, col, sid := bp2sid(sc.Text())
		seats[row][col] = sid
		if sid > highest {
			highest = sid
		}
	}
	myseat := 0
	prevseat := 0
	for _, r := range seats {
		for _, c := range r {
			if myseat == 0 && c != 0 {
				myseat = c
			} else if myseat != 0 && c == 0 {
				myseat = prevseat + 1
				goto found
			}
			prevseat = c
		}
	}
found:
	fmt.Println(myseat)
}

func bp2sid(bp string) (int, int, int) {
	r := dicho(bp[:7], 0, 127)
	c := dicho(bp[7:], 0, 7)
	return r, c, r*8 + c

}

func dicho(bin string, lower, upper int) int {
	var d int
	for _, b := range bin {
		d = (upper - lower) >> 1
		switch b {
		case 'F', 'L':
			upper -= d + 1
			d = upper
		case 'B', 'R':
			lower += d + 1
			d = lower
		}
	}
	return d
}
