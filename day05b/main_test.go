package main

import "testing"

func TestSID(t *testing.T) {
	tt := []struct {
		bp  string
		sid int
	}{
		{"BFFFBBFRRR", 567},
		{"FFFBBBFRRR", 119},
		{"BBFFBBFRLL", 820},
	}

	for _, tc := range tt {
		t.Run(tc.bp, func(t *testing.T) {
			if _, _, sid := bp2sid(tc.bp); sid != tc.sid {
				t.Fatalf("bad seat id conversion, expected %v, got %v", tc.sid, sid)
			}
		})
	}
}
