package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"strconv"
)

func main() {
	b, err := ioutil.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}
	buffer := bytes.NewBuffer(b)
	sc := bufio.NewScanner(buffer)
	expenses := make([]int, 0)
	for sc.Scan() {
		i, err := strconv.Atoi(sc.Text())
		if err != nil {
			panic(err)
		}
		expenses = append(expenses, i)
	}

	found := false
	for i, a := range expenses {
		for j, b := range expenses {
			if i != j && (a+b) == 2020 {
				fmt.Printf("%d + %d = 2020\n%d * %d = %d\n", a, b, a, b, a*b)
				found = true
				break
			}
		}
		if found {
			break
		}
	}
}
