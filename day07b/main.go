package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type bag struct {
	parent   map[string]*bag
	color    string
	children []child
}

type child struct {
	count int
	bag   *bag
}

func main() {
	if len(os.Args) < 2 {
		os.Exit(-1)
	}
	bags, err := loadbags(os.Args[1])
	if err != nil {
		panic(err)
	}
	sum := countcontainers(bags["shiny gold"])
	fmt.Println(sum)
	count := countchildren(bags["shiny gold"])
	fmt.Println(count - 1)
}

func countchildren(b *bag) int {
	count := 1
	for _, c := range b.children {
		count += c.count * countchildren(c.bag)
	}
	return count
}

func countcontainers(b *bag) int {
	parents := make(map[string]struct{})
	addparent(parents, b)
	return len(parents)
}

func addparent(parents map[string]struct{}, b *bag) {
	for _, p := range b.parent {
		addparent(parents, p)
		parents[p.color] = struct{}{}
	}
}

func loadbags(filename string) (map[string]*bag, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	sc := bufio.NewScanner(f)

	re := regexp.MustCompile(`([0-9]+ )?([a-z]+ [a-z]+) bag[s]?`)
	bags := make(map[string]*bag)
	for sc.Scan() {
		gs := re.FindAllStringSubmatch(sc.Text(), -1)
		// parent
		pc := gs[0][2]
		var p *bag
		var ok bool
		if p, ok = bags[pc]; !ok {
			p = &bag{color: pc, children: make([]child, 0)}
		}
		// parent
		// children
		for i := len(gs) - 1; i >= 1; i-- {
			var b *bag
			var ok bool
			c := gs[i][2]
			if b, ok = bags[c]; !ok {
				b = &bag{color: c}
			}
			if b.parent == nil {
				b.parent = make(map[string]*bag)
			}
			b.parent[p.color] = p
			bags[c] = b
			if gs[i][2] != "no other" {
				count, err := strconv.Atoi(strings.TrimSpace(gs[i][1]))
				if err != nil {
					log.Println(err)
				}
				p.children = append(p.children, child{count: count, bag: b})
			}
		}
		//children
		bags[pc] = p
	}
	return bags, nil
}
