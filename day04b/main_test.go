package main

import (
	"strconv"
	"testing"
)

func TestCheckPassport(t *testing.T) {
	tt := []struct {
		fields []string
		valid  bool
	}{
		{[]string{"eyr:1972", "cid:100", "hcl:#18171d", "ecl:amb", "hgt:170", "pid:186cm", "iyr:2018", "byr:1926"}, false},
		{[]string{"iyr:2019", "hcl:#602927", "eyr:1967", "hgt:170cm", "ecl:grn", "pid:012533040", "byr:1946"}, false},
		{[]string{"hcl:dab227", "iyr:2012", "ecl:brn", "hgt:182cm", "pid:021572410", "eyr:2020", "byr:1992", "cid:277"}, false},
		{[]string{"hgt:59cm", "ecl:zzz", "eyr:2038", "hcl:74454a", "iyr:2023", "pid:3556412378", "byr:2007"}, false},
		{[]string{"pid:087499704", "hgt:74in", "ecl:grn", "iyr:2012", "eyr:2030", "byr:1980", "hcl:#623a2f"}, true},
		{[]string{"eyr:2029", "ecl:blu", "cid:129", "byr:1989", "iyr:2014", "pid:896056539", "hcl:#a97842", "hgt:165cm"}, true},
		{[]string{"hcl:#888785", "hgt:164cm", "byr:2001", "iyr:2015", "cid:88", "pid:545766238", "ecl:hzl", "eyr:2022"}, true},
		{[]string{"iyr:2010", "hgt:158cm", "hcl:#b6652a", "ecl:blu", "byr:1944", "eyr:2021", "pid:093154719"}, true},
	}

	for i, tc := range tt {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			if valid := checkpassport(tc.fields); valid != tc.valid {
				t.Fatalf("passport validity failed, expected %v, got %v", tc.valid, valid)
			}
		})
	}
}
