package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	sc := bufio.NewScanner(f)
	var pp []string
	count := 0
	for sc.Scan() {
		if pp == nil {
			pp = make([]string, 0)
		}
		if sc.Text() == "" {
			if checkpassport(pp) {
				count++
			}
			pp = nil
			continue
		}
		pp = append(pp, strings.Fields(sc.Text())...)
	}
	if checkpassport(pp) {
		count++
	}
	fmt.Println(count)
}

func checkpassport(fields []string) bool {
	valids := make(map[string]struct{})
	for _, field := range fields {
		kv := strings.Split(field, ":")
		if fn, ok := required[kv[0]]; ok {
			if fn(kv[1]) {
				valids[kv[0]] = struct{}{}
			}
		}
	}
	return len(valids) == len(required)
}

type validfn func(string) bool

var required = map[string]validfn{
	"byr": byrvalidation,
	"iyr": iyrvalidation,
	"eyr": eyrvalidation,
	"hgt": hgtvalidation,
	"hcl": hclvalidation,
	"ecl": eclvalidation,
	"pid": pidvalidation,
}

func byrvalidation(value string) bool {
	v, err := strconv.Atoi(value)
	if err != nil {
		return false
	}
	return v >= 1920 && v <= 2002
}

func iyrvalidation(value string) bool {
	v, err := strconv.Atoi(value)
	if err != nil {
		return false
	}
	return v >= 2010 && v <= 2020
}

func eyrvalidation(value string) bool {
	v, err := strconv.Atoi(value)
	if err != nil {
		return false
	}
	return v >= 2020 && v <= 2030
}

func hgtvalidation(value string) bool {
	re := regexp.MustCompile(`([0-9]+)(cm|in)`)
	if a := re.FindStringSubmatch(value); a != nil {
		if len(a) > 3 {
			return false
		}
		v, err := strconv.Atoi(a[1])
		if err != nil {
			return false
		}
		switch a[2] {
		case "in":
			if v >= 59 && v <= 76 {
				return true
			}
		case "cm":
			if v >= 150 && v <= 193 {
				return true
			}
		}
	}
	return false
}

func hclvalidation(value string) bool {
	re := regexp.MustCompile(`#[0-9a-f]{6}`)
	return re.MatchString(value) && len(value) == 7
}

func eclvalidation(value string) bool {
	switch value {
	case "amb", "blu", "brn", "gry", "grn", "hzl", "oth":
		return true
	}
	return false
}

func pidvalidation(value string) bool {
	re := regexp.MustCompile(`[0-9]{9}`)
	return re.MatchString(value) && len(value) == 9
}
