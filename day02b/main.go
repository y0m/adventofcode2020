package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	sc := bufio.NewScanner(f)
	valid := 0
	for sc.Scan() {
		ok, err := checkpolicy(sc.Text())
		if err != nil {
			panic(err)
		}
		if ok {
			valid++
		}
	}
	fmt.Println(valid)
}

func checkpolicy(policy string) (bool, error) {
	fields := strings.Fields(policy)
	ranges := strings.Split(fields[0], "-")
	min, err := strconv.Atoi(ranges[0])
	if err != nil {
		return false, err
	}
	min--
	max, err := strconv.Atoi(ranges[1])
	if err != nil {
		return false, err
	}
	max--
	count := 0
	if fields[2][min] == fields[1][0] {
		count++
	}
	if fields[2][max] == fields[1][0] {
		count++
	}
	return count == 1, nil
}
