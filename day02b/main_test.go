package main

import "testing"

func TestCheckPolicy(t *testing.T) {
	tt := []struct {
		policy string
		valid  bool
	}{
		{"1-3 a: abcde", true},
		{"1-3 b: cdefg", false},
		{"2-9 c: ccccccccc", false},
	}

	for _, tc := range tt {
		t.Run(tc.policy, func(t *testing.T) {
			valid, err := checkpolicy(tc.policy)
			if err != nil {
				t.Fatalf("check policy error: %v", err)
			}
			if tc.valid != valid {
				t.Fatalf("expected policy validity to be %v; got %v", tc.valid, valid)
			}
		})
	}
}
