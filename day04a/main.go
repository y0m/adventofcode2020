package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	sc := bufio.NewScanner(f)
	var pp []string
	count := 0
	for sc.Scan() {
		if pp == nil {
			pp = make([]string, 0)
		}
		if sc.Text() == "" {
			if checkpassport(pp) {
				count++
			}
			pp = nil
			continue
		}
		pp = append(pp, strings.Fields(sc.Text())...)
	}
	if checkpassport(pp) {
		count++
	}
	fmt.Println(count)
}

var required = map[string]struct{}{
	"byr": struct{}{},
	"iyr": struct{}{},
	"eyr": struct{}{},
	"hgt": struct{}{},
	"hcl": struct{}{},
	"ecl": struct{}{},
	"pid": struct{}{},
}

func checkpassport(fields []string) bool {
	valids := make(map[string]struct{})
	for _, field := range fields {
		kv := strings.Split(field, ":")
		if _, ok := required[kv[0]]; ok {
			valids[kv[0]] = struct{}{}
		}
	}
	return len(valids) == len(required)
}
