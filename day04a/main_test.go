package main

import (
	"strconv"
	"testing"
)

func TestCheckPassport(t *testing.T) {
	tt := []struct {
		fields []string
		valid  bool
	}{
		{[]string{"ecl:gry", "pid:860033327", "eyr:2020", "hcl:#fffffd", "byr:1937", "iyr:2017", "cid:147", "hgt:183cm"}, true},
		{[]string{"iyr:2013", "ecl:amb", "cid:350", "eyr:2023", "pid:028048884", "hcl:#cfa07d", "byr:1929"}, false},
		{[]string{"hcl:#ae17e1", "iyr:2013", "eyr:2024", "ecl:brn", "pid:760753108", "byr:1931", "hgt:179cm"}, true},
		{[]string{"hcl:#cfa07d", "eyr:2025", "pid:166559648", "iyr:2011", "ecl:brn", "hgt:59in"}, false},
	}

	for i, tc := range tt {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			if valid := checkpassport(tc.fields); valid != tc.valid {
				t.Fatalf("passport validity failed, expected %v, got %v", tc.valid, valid)
			}
		})
	}
}
