package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type instruction struct {
	op  string
	arg int
	// debug
	exec int
}

func main() {
	if len(os.Args) < 2 {
		os.Exit(-1)
	}
	boot, err := loadboot(os.Args[1])
	if err != nil {
		panic(err)
	}
	acc := computeonce(boot)
	fmt.Println(acc)

	var twice bool
	acc, twice = testchange(boot, "nop")
	if !twice {
		fmt.Println("nop changes:", acc)
	}
	acc, twice = testchange(boot, "jmp")
	if !twice {
		fmt.Println("jmp changes:", acc)
	}
}

func testchange(boot []instruction, op string) (int, bool) {
	acc := 0
	oppos := 0
	twice := true
	opchg := ""
	if op == "nop" {
		opchg = "jmp"
	} else if op == "jmp" {
		opchg = "nop"
	}
	for i := 0; i < len(boot); i++ {
		if boot[i].op != op {
			continue
		}
		if op == "nop" && boot[i].arg == 0 {
			continue
		}
		oppos = i
		boot[i].op = opchg
		acc, twice = compute(boot)
		if !twice {
			break
		}
		boot[oppos].op = op
		for j := range boot {
			boot[j].exec = 0
		}
	}
	return acc, twice
}

func compute(boot []instruction) (int, bool) {
	pos := 0
	acc := 0
	for pos < len(boot) {
		inst := &(boot[pos])
		if inst.exec == 1 {
			return acc, true
		}
		switch inst.op {
		case "acc":
			acc += inst.arg
			pos++
		case "jmp":
			pos += inst.arg
		case "nop":
			pos++
		}
		inst.exec++
	}
	return acc, false
}

func computeonce(boot []instruction) int {
	pos := 0
	acc := 0
	for pos < len(boot) {
		inst := &(boot[pos])
		if inst.exec == 1 {
			break
		}
		switch inst.op {
		case "acc":
			acc += inst.arg
			pos++
		case "jmp":
			pos += inst.arg
		case "nop":
			pos++
		}
		inst.exec++
	}
	return acc
}

func loadboot(filename string) ([]instruction, error) {
	f, err := os.Open(os.Args[1])
	if err != nil {
		return nil, err
	}
	defer f.Close()
	sc := bufio.NewScanner(f)

	boot := make([]instruction, 0)
	line := 0
	for sc.Scan() {
		fields := strings.Fields(sc.Text())
		switch fields[0] {
		case "acc", "jmp", "nop":
			arg, err := strconv.Atoi(fields[1])
			if err != nil {
				return nil, fmt.Errorf("invalid arg line %d: %s", line, fields[1])
			}
			boot = append(boot, instruction{op: fields[0], arg: arg})
		default:
			return nil, fmt.Errorf("unknown op line %d: %s", line, fields[0])
		}
	}
	return boot, nil
}
