package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	if len(os.Args) < 1 {
		os.Exit(-1)
	}
	f, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}
	defer f.Close()

	sc := bufio.NewScanner(f)
	var group map[rune]int
	sum := 0
	count := 0
	for sc.Scan() {
		ans := sc.Text()
		if group == nil {
			group = make(map[rune]int)
		}
		if ans == "" {
			for _, c := range group {
				if c == count {
					sum++
				}
			}
			group = nil
			count = 0
			continue
		}
		for _, a := range ans {
			if _, ok := group[a]; !ok {
				group[a] = 0
			}
			group[a] = group[a] + 1
		}
		count++
	}
	for _, c := range group {
		if c == count {
			sum++
		}
	}
	fmt.Println(sum)
}
