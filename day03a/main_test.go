package main

import "testing"

func TestWalk(t *testing.T) {
	t.Run("input_test.txt", func(t *testing.T) {
		c, err := walk("input_test.txt")
		if err != nil {
			t.Fatalf("failed walk: %v", err)
		}
		if c != 7 {
			t.Fatalf("expected 7, got %v", c)
		}
	})
}
