package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	count, err := walk("input.txt")
	if err != nil {
		panic(err)
	}
	fmt.Println(count)
}

func walk(geology string) (int, error) {
	f, err := os.Open(geology)
	if err != nil {
		return -1, err
	}
	terrain := make([]string, 0)
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		terrain = append(terrain, sc.Text())
	}
	col := 0
	count := 0
	for i, t := range terrain {
		if i != 0 {
			col += 3
			col = col % len(t)
			if t[col] == '#' {
				count++
			}
		}
	}
	return count, nil
}
