package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	sc := bufio.NewScanner(f)
	var highest int
	for sc.Scan() {
		sid := bp2sid(sc.Text())
		if sid > highest {
			highest = sid
		}
	}
	fmt.Println(highest)
}

func bp2sid(bp string) int {
	return dicho(bp[:7], 0, 127)*8 + dicho(bp[7:], 0, 7)

}

func dicho(bin string, lower, upper int) int {
	var d int
	for _, b := range bin {
		d = (upper - lower) >> 1
		switch b {
		case 'F', 'L':
			upper -= d + 1
			d = upper
		case 'B', 'R':
			lower += d + 1
			d = lower
		}
	}
	return d
}
