package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	sc := bufio.NewScanner(f)
	valid := 0
	for sc.Scan() {
		ok, err := checkpolicy(sc.Text())
		if err != nil {
			panic(err)
		}
		if ok {
			valid++
		}
	}
	fmt.Println(valid)
}

func checkpolicy(policy string) (bool, error) {
	fields := strings.Fields(policy)
	ranges := strings.Split(fields[0], "-")
	min, err := strconv.Atoi(ranges[0])
	if err != nil {
		return false, err
	}
	max, err := strconv.Atoi(ranges[1])
	if err != nil {
		return false, err
	}
	count := 0
	for _, r := range fields[2] {
		if rune(fields[1][0]) == r {
			count++
		}
	}
	if count < min || count > max {
		return false, nil
	}
	return true, nil
}
