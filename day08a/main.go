package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type instruction struct {
	op  string
	arg int
	// debug
	exec int
}

func main() {
	if len(os.Args) < 2 {
		os.Exit(-1)
	}
	boot, err := loadboot(os.Args[1])
	if err != nil {
		panic(err)
	}
	acc := computeonce(boot)
	fmt.Println(acc)
}

func loadboot(filename string) ([]instruction, error) {
	f, err := os.Open(os.Args[1])
	if err != nil {
		return nil, err
	}
	defer f.Close()
	sc := bufio.NewScanner(f)

	boot := make([]instruction, 0)
	line := 0
	for sc.Scan() {
		fields := strings.Fields(sc.Text())
		switch fields[0] {
		case "acc", "jmp", "nop":
			arg, err := strconv.Atoi(fields[1])
			if err != nil {
				return nil, fmt.Errorf("invalid arg line %d: %s", line, fields[1])
			}
			boot = append(boot, instruction{op: fields[0], arg: arg})
		default:
			return nil, fmt.Errorf("unknown op line %d: %s", line, fields[0])
		}
	}
	return boot, nil
}

func computeonce(boot []instruction) int {
	pos := 0
	acc := 0
	for pos < len(boot) {
		inst := &(boot[pos])
		if inst.exec == 1 {
			break
		}
		switch inst.op {
		case "acc":
			acc += inst.arg
			pos++
		case "jmp":
			pos += inst.arg
		case "nop":
			pos++
		}
		inst.exec++
	}
	return acc
}
