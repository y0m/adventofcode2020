package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) < 3 {
		os.Exit(-1)
	}
	length, err := strconv.Atoi(os.Args[2])
	if err != nil {
		panic(err)
	}
	xmas, err := loadxmas(os.Args[1])
	if err != nil {
		panic(err)
	}
	if weak := testweakness(xmas, length); weak != -1 {
		fmt.Println(weak)
		if encw := encryptionweakness(xmas, weak); encw != -1 {
			fmt.Println(encw)
		}
	}
}

func encryptionweakness(xmas []int, weak int) int {
	for i := 0; i < len(xmas); i++ {
		sum := xmas[i]
		lowest := xmas[i]
		highest := xmas[i]
		for j := i + 1; j < len(xmas) && sum < weak; j++ {
			if highest < xmas[j] {
				highest = xmas[j]
			}
			if lowest > xmas[j] {
				lowest = xmas[j]
			}
			sum += xmas[j]
			if sum == weak {
				return lowest + highest
			}
		}
	}
	return -1
}

func testweakness(xmas []int, length int) int {
	if len(xmas) < length {
		return -1
	}
	for i := length; i < len(xmas); i++ {
		if weakness(xmas[i-length:i], xmas[i]) {
			return xmas[i]
		}
	}
	return -1
}

func weakness(xmas []int, number int) bool {
	for i := 0; i < len(xmas); i++ {
		for j := i + 1; j < len(xmas); j++ {
			if xmas[i]+xmas[j] == number {
				return false
			}
		}
	}
	return true
}

func loadxmas(filename string) ([]int, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	sc := bufio.NewScanner(f)
	xmas := make([]int, 0)
	for sc.Scan() {
		i, err := strconv.Atoi(sc.Text())
		if err != nil {
			return nil, err
		}
		xmas = append(xmas, i)
	}
	return xmas, nil
}
