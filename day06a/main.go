package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	if len(os.Args) < 1 {
		os.Exit(-1)
	}
	f, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}
	defer f.Close()

	sc := bufio.NewScanner(f)
	var group map[rune]struct{}
	sum := 0
	for sc.Scan() {
		ans := sc.Text()
		if group == nil {
			group = make(map[rune]struct{})
		}
		if ans == "" {
			sum += len(group)
			group = nil
			continue
		}
		for _, a := range ans {
			group[a] = struct{}{}
		}
	}
	sum += len(group)
	fmt.Println(sum)
}
