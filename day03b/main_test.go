package main

import (
	"testing"
)

func TestWalk(t *testing.T) {
	t.Run("input_test.txt", func(t *testing.T) {
		m, err := multiply("input_test.txt")
		if err != nil {
			t.Fatalf("failed multiply: %v", err)
		}
		if m != 336 {
			t.Fatalf("expected 336, got %v", m)
		}
	})
}
