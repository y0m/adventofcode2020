package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	count, err := multiply("input.txt")
	if err != nil {
		panic(err)
	}
	fmt.Println(count)
}

func multiply(geology string) (int, error) {
	f, err := os.Open(geology)
	if err != nil {
		return -1, err
	}
	defer f.Close()
	terrain := make([]string, 0)
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		terrain = append(terrain, sc.Text())
	}
	count := 1
	slopes := []struct {
		right, down int
	}{
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2},
	}
	for _, s := range slopes {
		c, err := slide(terrain, s.right, s.down)
		if err != nil {
			return -1, err
		}
		count *= c
	}
	return count, nil
}

func slide(terrain []string, right, down int) (int, error) {
	r := 0
	count := 0
	i := 0
	for i < len(terrain) {
		if i != 0 && i%down == 0 {
			r += right
			r = r % len(terrain[i])
			if terrain[i][r] == '#' {
				count++
			}
		}
		i++
	}
	return count, nil
}
