package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
)

type bag struct {
	parent map[string]*bag
	color  string
}

func main() {
	if len(os.Args) < 2 {
		os.Exit(-1)
	}
	bags, err := loadbags(os.Args[1])
	if err != nil {
		panic(err)
	}
	sum := countcontainers(bags["shiny gold"])
	fmt.Println(sum)
}

func countcontainers(b *bag) int {
	parents := make(map[string]struct{})
	addparent(parents, b)
	return len(parents)
}

func addparent(parents map[string]struct{}, b *bag) {
	for _, p := range b.parent {
		addparent(parents, p)
		parents[p.color] = struct{}{}
	}
}

func loadbags(filename string) (map[string]*bag, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	sc := bufio.NewScanner(f)

	re := regexp.MustCompile(`([a-z]+ [a-z]+) bag[s]?`)
	bags := make(map[string]*bag)
	for sc.Scan() {
		gs := re.FindAllStringSubmatch(sc.Text(), -1)
		// parent
		pc := gs[0][1]
		var p *bag
		var ok bool
		if p, ok = bags[pc]; !ok {
			p = &bag{color: pc}
		}
		// parent
		// children
		for i := len(gs) - 1; i >= 1; i-- {
			var b *bag
			var ok bool
			c := gs[i][1]
			if b, ok = bags[c]; !ok {
				b = &bag{color: c}
			}
			if b.parent == nil {
				b.parent = make(map[string]*bag)
			}
			b.parent[p.color] = p
			bags[c] = b
		}
		//children
		bags[pc] = p
	}
	return bags, nil
}
